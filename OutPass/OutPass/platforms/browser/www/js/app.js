// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'ngCordova', 'ngStorage', 'ionicLazyLoad', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'monospaced.qrcode', 'ionic-toast'])

  .config(['$compileProvider', function($compileProvider){
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|tel):/);
  }])

.run(function($ionicPlatform, $rootScope) {

  Parse.initialize('hb9Wuw1Pg96cFt1dWxceRqdHREZuodrGmPrtyIm9','c8dfgvHTkFSJEMP0G1bkUBsAgB4OXMyrJUbAgkt0');

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  });

  if (ionic.Platform.isIOS()) $rootScope.mapsUrl = 'http://maps.apple.com/?q=';
  else $rootScope.mapsUrl = 'http://maps.google.com/?q=';

});

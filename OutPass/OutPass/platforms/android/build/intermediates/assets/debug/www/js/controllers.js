angular.module('app.controllers', [])

.controller('loginCtrl', function($scope, $state) {

  $scope.data = {};
  $scope.authErr = false;

  $scope.signupEmail = function() {
    var user = new Parse.User();
    user.set("name", $scope.data.name);
    user.set("email", $scope.data.email);
    user.set("username", $scope.data.email);
    user.set("password", $scope.data.password);

    user.signUp(null, {
      success: function(user) {
        $scope.authErr = false;
        $state.go('menu.explore.today');
      },
      error: function(user, error) {
        $scope.authErr = true;
        $scope.$apply();
      }
    })
  };

  $scope.loginEmail = function(){
    Parse.User.logIn($scope.data.email, $scope.data.password, {
      success: function(user) {
        $scope.authErr = false;
        $state.go('menu.explore.today');
      },
      error: function(user, error) {
        $scope.authErr = true;
        $scope.$apply();
      }
    })
  }
})

.controller('exploreCtrl', function($scope, $ionicModal, $rootScope, $ionicLoading, $ionicPopover, $ionicPopup, $cordovaToast, EventServices, GuestPassServices, ionicToast) {

  //displays a loading indicator
  $ionicLoading.show({
    template: 'loading'
  });

  //loads events from server, and dismisses loader after
  EventServices.getEvents()
    .then(function(events){
      $scope.events = events;
    })
    .finally(function() {
      $ionicLoading.hide();
    });

  //handles pull to refresh
  $scope.refresh = function() {
    EventServices.refreshEvents()
      .then(function(events){
        $scope.events = events;
      })
      .finally(function() {
        $scope.$broadcast('scroll.refreshComplete');
      });
    GuestPassServices.getGuestPasses().then(function(data) {
      $scope.guestPasses = data;
    });
  };

  //event-viewer modal
  $ionicModal.fromTemplateUrl('templates/event-modal.html', {
    scope: $scope,
    animation: 'slide-in-right'
  }).then(function(modal) {
    $scope.modal = modal
  });

  $scope.openModal = function(event) {
    $scope.selEvent = event;
    $scope.modalLoading = true;
    $scope.hasPassErr = false;
    Parse.Cloud.run('hasPass', {eventId: event.ID}).then(function (results) {
      $scope.hasPass = results;
      $scope.modalLoading = false;
      $scope.$apply();
    }, function(err) {
      if (err.code == 100) {
        $scope.modalLoading = false;
        $scope.hasPassErr = true;
        $scope.$apply();
      }
    });
    $scope.modal.show();
    StatusBar.hide();
  };



  $scope.closeModal = function() {
    $scope.modal.hide();
    StatusBar.show();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  //end event-viewer modal

  //club-info popover
  $ionicPopover.fromTemplateUrl('templates/club-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };

  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });

  $scope.$on('popover.hidden', function() {

  });

  $scope.$on('popover.removed', function() {

  });
  //end club-info popover

  $scope.openPopoverExplore = function($event, event) {
    $scope.selEvent = event;
    $scope.openPopover($event);
  };

  //toast that confirms the addition of a GuestPass
  $scope.showToast = function(){
    <!-- ionicToast.show(message, position, stick, time); -->
    ionicToast.show('Successfully added GuestPass!', 'bottom', false, 2500);

  };

  $scope.hideToast = function(){
    ionicToast.hide();
  };
  //end toast

  //pop-up dialog to confirm GuestPass
  $scope.confirmGuestPass = function() {
    $scope.data={select: '0'};

    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirm GuestPass',
      subTitle: 'Please review the following pass details to confirm your pass',
      templateUrl: 'templates/guestpass-confirm.html',
      scope: $scope,
      buttons: [
        {
          text: 'Cancel',
          onTap: function() {
            return -1;
          }
        },
        {
          text: 'Confirm',
          type: 'button-assertive',
          onTap: function() {
            switch($scope.data.select){
              case '0': return 0;
              case '1': return 1;
              case '2': return 2;
              case '3': return 3;
              case '4': return 4;
            }
          }
        }
      ]
    });

    confirmPopup.then(function(res) {
      if (res == -1) {

      }
      else {
        $scope.addGuestPass(res);
      }
    });
  };

  //end pop-up dialog

  $scope.addGuestPass = function(plus) {
    GuestPassServices.addGuestPass($scope.selEvent, plus).then(function() {
      $scope.closeModal();
    }).finally(function() {
      $scope.showToast();
    });
  };

  $rootScope.$on('connection.error', function() {
    ionicToast.show('Not connected to the internet', 'bottom', true, 5000);
  });

  $scope.hideToast = function(){
    ionicToast.hide();
  };
})

.controller('myPassesCtrl', function($scope, $rootScope, $ionicLoading, $ionicPopover, EventServices, GuestPassServices, $ionicModal, ionicToast) {
  $ionicLoading.show({
    template: 'loading'
  });

  GuestPassServices.getGuestPasses()
    .then(function(data){
      $scope.guestPasses = data;
    })
    .finally(function() {
      $ionicLoading.hide();
      console.log($scope.guestPasses);
    });

  $ionicModal.fromTemplateUrl('templates/guestpass-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal
  });

  $scope.openModal = function(pass) {
    $scope.selPass = pass;
    $scope.selEvent = pass.event;
    $scope.modal.show();
    StatusBar.hide();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
    StatusBar.show();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $ionicPopover.fromTemplateUrl('templates/club-popover.html', {
    scope: $scope
  }).then(function(popover) {
    $scope.popover = popover;
  });

  $scope.openPopover = function($event) {
    $scope.popover.show($event);
  };
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  // Execute action on hide popover
  $scope.$on('popover.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popover.removed', function() {
    // Execute action
  });

  $rootScope.$on('guestPassAdded', function() {
    $scope.refresh();
  });

  $scope.refresh = function() {
    GuestPassServices.refreshGuestPasses()
      .then(function(data){
        $scope.guestPasses = data;
      })
      .finally(function() {
        $scope.$broadcast('scroll.refreshComplete');
      });
  };

  $rootScope.$on('connection.error', function() {
    ionicToast.show('Not connected to the internet', 'bottom', true, 5000);
  });

  $scope.hideToast = function(){
    ionicToast.hide();
  };

})

.controller('myProfileCtrl', function($scope, $window) {

  var currentUser = Parse.User.current();
  $scope.name = currentUser.get('name');
  $scope.username = currentUser.get('username');
  $scope.email = currentUser.get('email');

  $scope.logout = function() {
    Parse.User.logOut();

    // instead of using $state.go, we're going to redirect.
    // reason: we need to ensure views aren't cached.
    $window.location.href = 'index.html';
  }
});


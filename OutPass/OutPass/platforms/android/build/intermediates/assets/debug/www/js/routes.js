angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



    .state('signin', {
      url: '/signin',
      templateUrl: 'templates/signin.html',
      controller: 'loginCtrl'
    })





    .state('signup', {
      url: '/signup',
      templateUrl: 'templates/signup.html',
      controller: 'loginCtrl'
    })





    .state('menu.explore.today', {
      url: '/today',
      views: {
        'tab1': {
          templateUrl: 'templates/today.html',
          controller: 'exploreCtrl'
        }
      }
    })





    .state('menu.explore.thisWeek', {
      url: '/thisweek',
      views: {
        'tab2': {
          templateUrl: 'templates/thisWeek.html',
          controller: 'exploreCtrl'
        }
      }
    })





    .state('menu.explore.upcoming', {
      url: '/upcoming',
      views: {
        'tab3': {
          templateUrl: 'templates/upcoming.html',
          controller: 'exploreCtrl'
        }
      }
    })


    .state('menu.explore', {
      url: '/explore',
      views: {
        'side-menu21': {
          abstract:true,
          templateUrl: 'templates/explore.html'
        }
      }
    })

    .state('splash', {
      url: '/intro',
      templateUrl: 'templates/outPass.html',
      controller: 'loginCtrl',
      onEnter: function($state){
        var usr = Parse.User.current();
        if (usr) $state.go('menu.explore.today');
      }
    })




    .state('menu', {
      url: '/side-menu',
      abstract:true,
      templateUrl: 'templates/menu.html',
      onEnter: function($state){
          var usr = Parse.User.current();
          if (!usr) $state.go('splash');
        }
    })




    .state('menu.myPasses', {
      url: '/mypasses',
      views: {
        'side-menu21': {
          templateUrl: 'templates/myPasses.html',
          controller: 'myPassesCtrl'
        }
      }
    })





    .state('menu.myProfile', {
      url: '/profile',
      views: {
        'side-menu21': {
          templateUrl: 'templates/myProfile.html',
          controller: 'myProfileCtrl'
        }
      }
    })


    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/intro');

});

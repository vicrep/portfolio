angular.module('app.services', [])

  .factory('EventServices', function($q, $localStorage, $rootScope){
    var o = null;

    return {
      loadEvents: function() {
        var d = $q.defer();

        Parse.Cloud.run('getEventsParsed',{}, {
          success: function(events) {
            o = events;
            console.log("successfully pulled events from server");
            $localStorage.events = events;
            d.resolve(o);
          },
          error: function(err) {
            if (err.code = 100) {
              o = $localStorage.events;
              $rootScope.$broadcast('connection.error');
              d.resolve(o);
            }
          }
        });

        return d.promise;
      },
      refreshEvents: function() {
        var d = $q.defer();

        this.loadEvents().then(function(data) {
          d.resolve(data);
        });
        return d.promise;
      },
      getEvents: function() {
        var d = $q.defer();


        if (!o) this.loadEvents().then(function(data) {
          d.resolve(data);
        });
        else {
          d.resolve(o);
        }
        return d.promise;
      }
    }

  })
  .factory('GuestPassServices', function($q, $localStorage, $rootScope) {
    var o = null;

    return {
      addGuestPass: function(event, plus) {
        var d = $q.defer();

        console.log(event.ID);
        Parse.Cloud.run('addGuestPass', { eventId: event.ID, plus: plus},
          {
            success: function(data) {
              $rootScope.$broadcast('guestPassAdded');
              d.resolve();
            }
          });
        return d.promise;
      },
      getGuestPasses: function() {
        var d = $q.defer();

        if (!o) this.loadGuestPasses().then(function(data) {
          o = data;
          d.resolve(o);
        });
        else d.resolve(o);
        return d.promise;
      },
      refreshGuestPasses: function() {
        var d = $q.defer();

        this.loadGuestPasses().then(function(data) {
          o = data;
          d.resolve(o);
        });
        return d.promise;
      },
      loadGuestPasses: function() {
        var d = $q.defer();

        Parse.Cloud.run('getGuestPasses', {}, {
          success: function(data) {
            console.log("successfully pulled GuestPasses from server");
            $localStorage.guestPasses = data;
            o = data;
            d.resolve(o);
          },
          error: function(err) {
            if(err.code == 100) {
              o = $localStorage.guestPasses;
              $rootScope.$broadcast('connection.error');
              d.resolve(o);
            }
          }
        });
        return d.promise;
      }
    }
  })
  .directive('convertToNumber', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(val) {
          return parseInt(val, 10);
        });
        ngModel.$formatters.push(function(val) {
          return '' + val;
        });
      }
    };
  });

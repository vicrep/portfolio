/**
 * Automatically generated file. DO NOT MODIFY
 */
package co.outpass.partners;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "co.outpass.partners";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1018;
  public static final String VERSION_NAME = "0.1.1";
}

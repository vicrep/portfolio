angular.module('app.routes', [])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise(function(){
      if (!Parse.User.current()) return '/signin';
      else return '/events';
    });

    $stateProvider
      .state('signin', {
        url: '/signin',
        views: {
          'main': {
            templateUrl: 'templates/signin.html',
            controller: 'SignInCtrl',
            onEnter: function($state, AuthServices) {
              if (AuthServices.isAuth()) $state.go('events.select');
            }
          }
        }
      })
      .state('events', {
        url: '/events',
        abstract: true,
        views: {
          'main': {
            templateUrl: 'templates/events-template.html',
            controller: 'EventsCtrl',
            onEnter: function($state, AuthServices) {
              if (!AuthServices.isAuth()) $state.go('signin');
            }
          }
        }
      })
      .state('events.select', {
        url: '',
        views: {
          'events': {
            templateUrl: 'templates/events.html'
          }
        }
      })
      .state('events.guestpasses', {
        url: '/:eventId',
        views: {
          'events': {
            templateUrl: 'templates/guestpasses.html',
            controller: 'GuestPassesCtrl'
          }
        },
        resolve: {
          passes: function($stateParams, GuestPassServices) {
            console.log($stateParams.eventId);
            return GuestPassServices.getPasses($stateParams.eventId);
          }
        }
      })
  });

angular.module('app.services', [])
  .factory('AuthServices', function($q) {


    return {
      isAuth: function() {
        return Parse.User.current();
      },
      signIn: function(email, password) {
        var d = $q.defer();
        Parse.User.logIn(email, password, {
          success: function(user) {
            if (user.get('club')) d.resolve();
            else d.reject('This account is not associated with a club');
          },
          error: function(user, error) {
            d.reject('Invalid email or password')
          }
        });
        return d.promise;
      },
      signOut: function() {
        Parse.User.logOut();
      }
    }
  })
  .factory('EventServices', function($q) {
    var events = [];

    return {
      loadEvents: function() {
        var d = $q.defer();
        console.log('pulling events from server...');
        Parse.Cloud.run('getClubEvents', {}, {
          success: function(data) {
            console.log(data);
            events = data;
            d.resolve(data);
          },
          error: function(error) {
            d.reject(error);
          }
        });
        return d.promise;
      },
      getEvents: function() {
        var d = $q.defer();
        if(events.length == 0) this.loadEvents().then(function(data) {
          d.resolve(data);
          console.log('data');
        });
        else d.resolve(events);
        return d.promise;
      }
    }
  })
  .factory('GuestPassServices', function($q) {

    return {
      getPasses: function(eventId) {
        var d = $q.defer();
        Parse.Cloud.run('getEventPasses', {eventId: eventId}, {
          success: function(data) {
            d.resolve(data);
          },
          error: function(error) {
            d.reject(error);
          }
        });
        return d.promise;
      },
      scanPass: function(passId) {
        var d = $q.defer();
        Parse.Cloud.run('scanPass', {passId: passId}, {
          success: function(data) {
            d.resolve(data);
          },
          error: function(error) {
            d.reject(error);
          }
        });
        return d.promise;
      },
      toggleRedeemed: function(passId) {
        var d = $q.defer();
        Parse.Cloud.run('togglePass', {passId: passId}, {
          success: function(data) {
            d.resolve(data);
          },
          error: function(error) {
            d.reject(error);
          }
        });
        return d.promise;
      },
      setPlus: function(passId, plus) {
        var d = $q.defer();
        Parse.Cloud.run('setPlus', {
          passId: passId,
          plus: plus
        }, {
          success: function() {
            d.resolve();
          },
          error: function(error) {
            d.reject(error);
          }
        });
        return d.promise;
      }

    }

  });

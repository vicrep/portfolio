angular.module('app.controllers' ,[])
  .controller('SignInCtrl', function($scope, $state, AuthServices) {
    $scope.data = {
      email: '',
      password: ''
    };

    $scope.signIn = function() {
      AuthServices.signIn($scope.data.email, $scope.data.password)
        .then(function() {
          $state.go('events.select');
        }, function(error) {
          $scope.error = error;
          console.log(error);
        });

    }
  })
  .controller('EventsCtrl', function($scope, $state, $window, $ionicLoading, AuthServices, EventServices) {
    $scope.signOut = function() {
      AuthServices.signOut();
      $window.location.href = '';
    };

    $ionicLoading.show({
      template: 'loading'
    });

    EventServices.getEvents().then(function(data) {
      console.log('got events: ' + data);
      $scope.events = data;
    }).finally(function() {
      $ionicLoading.hide();
    });

    $scope.seeGuestPass = function(eventId) {
      $state.go('events.guestpasses', {eventId: eventId});
    }

  })

  .controller('GuestPassesCtrl', function($scope, $stateParams, passes, $cordovaBarcodeScanner, GuestPassServices, $ionicModal, $ionicPopup) {
    $scope.guestPasses = passes;

    $ionicModal.fromTemplateUrl('templates/guestpass-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal
    });

    $scope.openModal = function() {
      $scope.modal.show()
    };

    $scope.closeModal = function() {
      $scope.guestPass = {};
      $scope.modal.hide();
    };

    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    $scope.manualValidation = function(passID, pass) {
      $scope.hasVerified = false;
      $scope.guestPass = {};
      GuestPassServices.toggleRedeemed(passID).then(function(data) {
        $scope.guestPass = data;
      }).finally(function() {
        $scope.hasVerified = true;
        if (!$scope.guestPass.hasRedeemed) $scope.showPopup(pass);
        $scope.refresh();
      });
    };

    $scope.refresh = function() {
      GuestPassServices.getPasses($stateParams.eventId).then(function(data) {
        $scope.guestPasses = data;
      }).finally(function() {
        $scope.$broadcast('scroll.refreshComplete');
      });
    };

    $scope.scanBarcode = function() {
      $cordovaBarcodeScanner.scan().then(function(imageData) {
        if(imageData.text) {
          $scope.hasVerified = false;
          $scope.guestPass = {};
          $scope.openModal();
          GuestPassServices.scanPass(imageData.text).then(function(data) {
            $scope.guestPass = data;
          }).finally(function() {
            $scope.hasVerified = true;
            $scope.refresh();
          });
        }
      }, function(error) {
        console.log("An error happened -> " + error);
      });
    };

    $scope.setPlus = function(passId, plus) {
      GuestPassServices.setPlus(passId, plus).then(function() {
      }).finally(function() {
        $scope.closeModal();
      });
    };

    // Triggered on a button click, or some other target
    $scope.showPopup = function(pass) {
      // An elaborate, custom popup
      var extraGuestsPopup = $ionicPopup.show({
        template: '<input type="password" ng-model="data.wifi">',
        title: 'Enter number of Extra Guests',
        subTitle: 'Registered Extra Guests: ' + pass.plus,
        scope: $scope,
        buttons: [
          {
            text: '0',
            type: 'button-small button-dark',
            onTap: function() {
              return 0;
            }
          },
          {
            text: '1',
            type: 'button-small button-dark',
            onTap: function() {
              return 1;
            }
          },
          {
            text: '2',
            type: 'button-small button-dark',
            onTap: function() {
              return 2;
            }
          },
          {
            text: '3',
            type: 'button-small button-dark',
            onTap: function() {
              return 3;
            }
          },
          {
            text: '4',
            type: 'button-small button-dark',
            onTap: function() {
              return 4;
            }
          }
        ]
      });

      extraGuestsPopup.then(function(res) {
        GuestPassServices.setPlus(pass.passId, res).then(function() {
        });
      });
    };

});

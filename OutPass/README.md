## OutPass
*Reproduced for showcase purposes with the consent of all co-founders*

This is the repo for a startup that I co-founded in August 2015 with 2 friends.

OutPass's main vision is to create an novel way for users to discover events happening in clubs and bars around their cities, and to easily get access to guestlists and promotions exclusive to the app. OutPass also aims to simplify the process of claiming said guestlists and promotions, by allowing club door staff to scan in our customers by using a partner app (using QR codes), and also allowing them access to a centralized guestlist that is synced across all the staff's apps. Finally, we aim to help clubs better understand their clients by providing analytics on users of the app, on an event and venue basis.

The team currently consists of 5 members:

- 3 co-founders (2 business students and myself as technical lead)
- 1 developer
- 1 designer

The apps been pre-launched on their respective app stores pending an imminent soft launch, and can be viewed here:

- [iOS App Store Dev Page](https://itunes.apple.com/ca/developer/victor-repkow/id1079104244)
- [Google Play Dev Page](https://play.google.com/store/apps/dev?id=5568608686151600485)

We currently have 8-10 clubs (all Montreal based) who have partnered up with us at launch.

###The Code
There are three components to this repository. Note that all code as of yet has been developed exclusively by myself, as we only hired our first developer in the end of Feb '16.

**OutPass / OutPassPartners**
Respectively contain the source for the customer and partner apps.

Both apps were built using [AngularJS](https://angularjs.org/) and the [Ionic Framework](http://ionicframework.com/). The main source of the apps can be found in their `/www` directory.

**Parse**
We implemented the app's backend using the Parse BaaS. Most of the model logic resides is implemented using Parse's 'Cloud Code' which is an ExpressJS-derived SDK. This code can be found in the `/cloud` directory.
*The `/public` directory contains a web-version of the app, used for initial stress testing. This can be accessed at [app.outpass.co](http://app.outpass.co/).*


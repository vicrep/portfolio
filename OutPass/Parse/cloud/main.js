//Parse Cloud Code for the OutPass App

var moment = require('cloud/moment/moment-timezone-with-data.js');
moment.tz.setDefault("America/New_York");

Parse.Cloud.useMasterKey();

var parseEvent = function(event) {
    var club = event.get('club');
    return {
        name: event.get('name'),
        location: event.get('location'),
        date: moment(event.get('date')).format("ddd, MMM Do"),
        price: event.get('price'),
        priceCondition: event.get('priceCondition'),
        description: event.get('description'),
        img: event.get('img').url(),
        ID: event.id,
        startTime: moment(event.get('date')).format("h:mma"),
        club: {
            name: club.get('name'),
            description: club.get('description'),
            phone: club.get('phone'),
            fb: club.get('facebook'),
            logo: club.get('logo').url(),
            address: club.get('address')
        },
        offer: event.get('offer'),
        offerCondition: event.get('offerCondition')
    }
};

Date.prototype.sameDay = function() {
    var d = new Date();
    return this.getFullYear() === d.getFullYear()
        && this.getDate() === d.getDate()
        && this.getMonth() === d.getMonth();
};

Date.prototype.sameWeek = function() {
    var d = new Date();
    d.setDate(d.getDate() + 7);
    return (this <= d);
};

Date.prototype.upcoming = function() {
    var d = new Date();
    return this.getFullYear() >= d.getFullYear()
        && this.getDate() >= d.getDate()
        && this.getMonth() >= d.getMonth();
};

var sameDay = function(date) {
    return moment(date).isSameOrBefore(moment().add(1, 'd'));
};

var sameWeek = function(date) {
    return moment(date).isSameOrBefore(moment().add(7, 'd'));
};

var upcoming = function(date) {
    return moment(date).endOf('day').add(3, 'h').isSameOrAfter();
};

Parse.Cloud.define('getEvents', function(request, response) {

    var events = {
        today: [],
        thisweek: [],
        upcoming: []
    };

    var eventQuery = new Parse.Query('Event');
    eventQuery.limit(1000);
    eventQuery.ascending('date');
    eventQuery.include('club');

    eventQuery.find({
        success: function (data) {
            for (var i = 0; i < data.length; i++) {
                var date = data[i].get('date');
                if (upcoming(date)) {
                    if (sameDay(date)) {
                        events.today.unshift(data[i])
                    }

                    if (sameWeek(date)) {
                        events.thisweek.unshift(data[i]);
                    }

                    events.upcoming.unshift(data[i]);
                }
            }

            response.success(events);
        },
        error: function (error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define('getEventsParsed', function(request, response) {
    var parsedEvents = {
        today: [],
        thisweek: [],
        upcoming: []
    };

    Parse.Cloud.run('getEvents', {}, {
        success: function(events) {
            for (var i = 0; i < events.today.length; i++) {
                parsedEvents.today.unshift(parseEvent(events.today[i]))
            }
            for (var i = 0; i < events.thisweek.length; i++) {
                parsedEvents.thisweek.unshift(parseEvent(events.thisweek[i]))
            }
            for (var i = 0; i < events.upcoming.length; i++) {
                parsedEvents.upcoming.unshift(parseEvent(events.upcoming[i]))
            }
            response.success(parsedEvents);
        },
        error: function(error) {
            response.error(error);
        }
    });
});

Parse.Cloud.define('addGuestPass', function(request, response) {

    Parse.Cloud.useMasterKey();

    var promises = [];

    var addGuestPass = function() {

        var promise = new Parse.Promise();

        var findEvent = new Parse.Query('Event');

        var GuestPass = Parse.Object.extend('GuestPass');
        var guestPass = new GuestPass();

        findEvent.get(request.params.eventId)
            .then(function(event) {
                guestPass.set('user', request.user);
                guestPass.set('event', event);
                guestPass.set('hasRedeemed', false);
                guestPass.set('plus', request.params.plus);
                promise.resolve(guestPass.save());
            }, function(error) {
                promise.reject(error);
            });

        return promise;
    };

    promises.push(addGuestPass());

    Parse.Promise.when(promises).then(function(data) {
        response.success('Successfully added GuestPass');
    }, function(error) {
        response.error('Error adding new GuestPass : ' + error);
    });

});

Parse.Cloud.define('getGuestPasses', function(request, response){
    var guestPasses = {
        active: [],
        redeemed: []
    };

    var userQuery = new Parse.Query('GuestPass');

    userQuery.include('event');
    userQuery.include('event.club');

    userQuery.equalTo('user', request.user);

    userQuery.find({
        success: function(data) {
            data.sort(function(a, b) {
                var dA = moment(a.get('event').get('date'));
                var dB = moment(b.get('event').get('date'));
                if (moment(dA).isAfter(dB)) {
                    return -1;
                }
                else if (moment(dA).isBefore(dB)) return 1;
                else return 0;
            });

            for(var i = 0; i < data.length; i++) {
                var guestPass = {
                    ID: data[i].id,
                    eventId: data[i].get('event').id,
                    event: parseEvent(data[i].get('event')),
                    plus: data[i].get('plus')
                };

                if (data[i].get('hasRedeemed')) guestPasses.redeemed.unshift(guestPass);
                else if(upcoming(data[i].get('event').get('date'))) guestPasses.active.unshift(guestPass);
            }
            response.success(guestPasses);

        },
        error: function(error) {
            console.log(error);
        }
    });
});

Parse.Cloud.define('getEventPasses', function(request, response) {

    var promises = [];

    var eventQuery = new Parse.Query('Event');
    var passQuery = new Parse.Query('GuestPass');
    var guestPasses = [];
    passQuery.include('user');


    var getPasses = eventQuery.get(request.params.eventId).then(function(data) {
        passQuery.equalTo('event', data);
        return passQuery.find();
    }).then(function(data) {
        p = new Parse.Promise();
        for (var i = 0; i < data.length; i ++) {
            guestPasses.unshift( {
                name: data[i].get('user').get('name'),
                plus: data[i].get('plus'),
                actualPlus: data[i].get('actualPlus'),
                passId: data[i].id,
                hasRedeemed: data[i].get('hasRedeemed')
            })
        }
        guestPasses.sort(function(a, b) {
            if (a.name < b.name) return -1;
            else if (a.name > b.name) return 1;
            else return 0;
        });
        p.resolve(guestPasses);
        return p;
    });

    promises.push(getPasses);

    Parse.Promise.when(promises).then(function(data) {
        response.success(data);
    });

});

Parse.Cloud.define('getClubEvents', function(request, response) {
    var events = [];
    var club = request.user.get('club');
    var eventQuery = new Parse.Query('Event');
    eventQuery.include('club');
    eventQuery.ascending('date');
    eventQuery.equalTo('club', club);
    eventQuery.find({
        success: function(data) {
            for (var i = 0; i < data.length; i++) {
                events.push(parseEvent(data[i]));
            }
            response.success(events);
        },
        error: function(error) {
            response.error(error);
        }
    });

});

Parse.Cloud.define('scanPass', function(request, response) {
    var query = new Parse.Query('GuestPass');
    query.include('user');
    query.get(request.params.passId, {
        success: function(pass) {
            var data = {
                passId: pass.id,
                name: pass.get('user').get('name'),
                plus: pass.get('plus'),
                hasRedeemed: pass.get('hasRedeemed')
            };
            if(!data.hasRedeemed) {
                pass.set('hasRedeemed', !data.hasRedeemed);
                pass.set('timeRedeemed', new Date());
            }

            pass.save();
            response.success(data);
        },
        error: function() {
            response.error('Invalid Pass (not found)');
        }
    });
});

Parse.Cloud.define('togglePass', function(request, response) {
    var query = new Parse.Query('GuestPass');
    query.include('user');
    query.get(request.params.passId, {
        success: function(pass) {
            var data = {
                passId: pass.id,
                name: pass.get('user').get('name'),
                plus: pass.get('plus'),
                hasRedeemed: pass.get('hasRedeemed')
            };

            pass.set('hasRedeemed', !data.hasRedeemed);
            if(data.hasRedeemed) pass.set('timeRedeemed', new Date());
            pass.save();
            response.success(data);
        },
        error: function() {
            response.error('Invalid Pass (not found)');
        }
    });
});

Parse.Cloud.define('setPlus', function(request, response) {
    var query = new Parse.Query('GuestPass');
    query.get(request.params.passId, {
        success: function(pass) {
            pass.set('actualPlus', request.params.plus);
            pass.save();
            response.success();
        },
        error: function(error) {
            response.error('GuestPass not found');
        }
    })
});

Parse.Cloud.define('hasPass', function(request, response) {
    var Event = Parse.Object.extend('Event');
    var thisEvent = new Event();
    thisEvent.id = request.params.eventId;

    var query = new Parse.Query('GuestPass');
    query.equalTo('user', request.user);
    query.equalTo('event', thisEvent);
    query.count({
        success: function(count) {
            if (count > 0) response.success(true);
            else response.success(false);
        },
        error: function(err) {
            response.error(err);
        }
    })
});
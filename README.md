#Victor Repkow -- Development Portfolio
 - repkowvictor@gmail.com
 - +1 (438) 402 8586
 - https://ca.linkedin.com/in/repkowvictor
 - https://github.com/vicrep

This repository aims at representing excerpts of my development work over the past few years.
###Repository Directory
*all directories have their respective README.md files with specific instructions and descriptions*

	/McHacks-FFS          -- (Angular2/Ionic2/Firebase) Hybrid mobile app 
						     developed for McGill's 2016 Hackathon
	/OutPass              -- Central Repo for OutPass (startup I'm co-founding)
		/OutPass          -- (AngularJS/Ionic) Consumer mobile app
		/OutPassPartners  -- (AngularJS/Ionic) Partner mobile app
		/Parse            -- (Parse/ExpressJS) Backend code
	/SoccerScoringLib     -- (Java/Docs) University project, java library/framework 
							 for scoring soccer matches
	/SudokuSolver         -- (Java) University project, implementation of a sudoku
							 solving algorithm
	 
							 


##FFS - A campus-based exchange/sale platform
This is a hybrid mobile app developed by myself and a friend as part of McGill's 2016 Hackathon. Please note that this was developed in its entirety in 24 consecutive hours, so it still has some rough edges.


We developed this application using [Angular 2](https://angular.io/) and the [Ionic 2 framework](http://ionicframework.com/docs/v2/) (both were in beta at the time). We also used Google's FireBase as a backend.


My roles in this application were mostly app view and control implementation, as well as roughly half of the domain logic. My friend designed the domain model and the other half of the domain logic.


Source files for the app can be found under the `/app` directory.


Please visit the project's [GitHub](https://github.com/vicrep/McHacks-FFS) and [Devpost page](http://devpost.com/software/mchacks-ffs) for more information and screenshots.

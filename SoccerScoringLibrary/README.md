## SocScore Library
This was developed as part of a 4-person group project for a McGill Software Engineering class.

The project's goal was to design and create a soccer scoring application using common software design patterns and methods, such as MVC and Abstract Factory Objects. The app was required to run natively on Desktops (JavaSwing), Android, and Browsers (using PHP).

While the other 3 member focused on the applications themselves, I was put in charge of designing and implementing a framework/library to increase cohesion in our project. This was not a requirement but rather a strategic decision taken by my group. Hence, 99% of what is present in this directory was created by me.

####About the Code

The code for this framework was initially developed in Java 8 (to make use of lambdas and functional programming features), but I unfortunately had to convert it down to Java 7 due to the Android SDK not supporting Java 8.

The code is fully documented using JavaDocs, which can be found in the `/docs` directory. Source files for the code can be found in the `/src` directory. 
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;


class Sudoku
{
    /* SIZE is the size parameter of the Sudoku puzzle, and N is the square of the size.  For 
     * a standard Sudoku puzzle, SIZE is 3 and N is 9. */
    final int SIZE, N;

    /* The grid contains all the numbers in the Sudoku puzzle.  Numbers which have
     * not yet been revealed are stored as 0. */
    final int Grid[][];

    boolean isAllowed[][][];    //for every position on the grid, this stores a boolean for every possible value indicating
                                // whether it can be placed at that position of the grid

    int[][] validCount;         //stores the number of valid values a specific location of the grid can take (used in backtracking)


    //main solve method, calls all other submethods defined below
    public void solve() {
        //initializes the isAllowed array
        initAllowed();
        //runs 'rules' as those are more efficient than backtracking
        ruleSolvers();
        //if the grid is not solved with the rules, then the backtracking algorithm is called
        if(!isSolved()) {
            validCount = new int[N][N]; //initialize the counting array for the backtracking algorithm
            backtrackSolveOrder();
        }
    }


    //this method initializes the isAllowed[][][] 3d array, storing which values are allowed to be placed in every
    //empty position, and setting all non-empty positions to false (to indicate we can't insert any values in them)
    public void initAllowed() {
        isAllowed = new boolean[N][N][N];
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                Arrays.fill(isAllowed[i][j], false);
            }
        }
        for(int i = 0; i < N; i++) {
            for(int j = 0; j < N; j++) {
                int value = Grid[i][j];
                if(isEmpty(value)) {
                    for(int v = 0; v < N; v++) {
                        isAllowed[i][j][v] = isValid(i, j, v+1);
                    }
                }
            }
        }
    }


    //this method sets a value at the specified position, and updates the other members of this
    //position's row, column, and box to reflect that this value cannot be placed anywhere else
    //in those sections
    public void setAllowed(int x, int y, int value) {

        Grid[x][y] = value;

        //updating dependencies for the row and column
        for(int i = 0; i < N; i++) {
            isAllowed[x][i][value-1] = false;
            isAllowed[i][y][value-1] = false;
        }


        //updating the dependency for the section (sub-grid) the specified position is in
        int xBox = (x / SIZE) * SIZE, yBox = (y / SIZE) * SIZE;

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                isAllowed[xBox + i][yBox + j][value-1] = false;
            }
        }


        //updating the specified position to reflect that no other values may be placed there
        for(int i = 0; i < N; i++) {
            isAllowed[x][y][i] = false;
        }

    }
    //this method calls the two 'rules' below until neither are able to place any more values
    public void ruleSolvers() {
        boolean isSolved = true;
        while(isSolved) {
            isSolved = isAllowedSolver();
            isSolved = isSolved || isAllowedSectionSolver();
        }
    }


    //this method checks whether a specific location can only take a single value, and if it can,
    //places this value in the location, and updates the dependencies with setValue. It returns
    //true if a value was successfully placed
    public boolean isAllowedSolver() {
        boolean solvedGrid = false;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (isEmpty(Grid[i][j])) {
                    int count = 0, valIndex = 0;
                    for(int k = 0; k < N; k++) {
                        if (isAllowed[i][j][k]) {
                            count ++;
                            valIndex = k;
                        }
                    }
                    if (count == 1) {
                        setAllowed(i, j, valIndex + 1);
                        solvedGrid = true;
                    }

                }
            }
        }
        return solvedGrid;
    }


    //this method checks whether a value can be inserted based on whether it is unique in the row,
    //column, and box (sub-grid) where we are trying to place it. It also updates dependencies with setValue.
    //It returns true if a value was successfully placed.
    public boolean isAllowedSectionSolver() {
        boolean isSolvedSection = false;

        //Checking for single possible values in rows
        for(int i = 0; i < N; i++) {
            int rowCount[] = new int[N];
            Arrays.fill(rowCount, 0);
            for(int j = 0; j < N; j++) {
                int value = Grid[i][j];
                if(isEmpty(value)) {
                    for(int v = 0; v < N; v++) {
                        if(isAllowed[i][j][v]) {
                            rowCount[v]++;
                        }
                    }
                }
                else rowCount[value-1] = 2;
            }
            for(int v = 0; v < N; v++) {
                if(rowCount[v] == 1) {
                    for(int j = 0; j < N; j++) {
                        if(isAllowed[i][j][v]) {
                            setAllowed(i, j, v+1);
                            isSolvedSection = true;
                        }
                    }
                }
            }
        }

        //Checking for single possible values in columns
        for(int j = 0; j < N; j++) {
            int colCount[] = new int[N];
            Arrays.fill(colCount, 0);
            for(int i = 0; i < N; i++) {
                int value = Grid[i][j];
                if(isEmpty(value)) {
                    for(int v = 0; v < N; v++) {
                        if(isAllowed[i][j][v]) {
                            colCount[v]++;
                        }
                    }
                }
                else colCount[value-1] = 2;
            }
            for(int v = 0; v < N; v++) {
                if(colCount[v] == 1) {
                    for(int i = 0; i < N; i++) {
                        if(isAllowed[i][j][v]) {
                            setAllowed(i, j, v+1);
                            isSolvedSection = true;
                        }
                    }
                }
            }
        }

        //checking for single possible values in boxes
        for(int xBox = 0; xBox < N; xBox += SIZE) {
            for(int yBox = 0; yBox < N; yBox += SIZE) {
                int boxCount[] = new int[N];
                Arrays.fill(boxCount, 0);
                for(int i = 0; i < N; i++) {
                    int x = xBox + i%SIZE;
                    int y = yBox + i/SIZE;
                    int value = Grid[x][y];
                    if(isEmpty(value)) {
                        for(int v = 0; v < N; v++) {
                            if(isAllowed[x][y][v]) {
                                boxCount[v]++;
                            }
                        }
                    }
                    else boxCount[value-1] = 2;
                }
                for(int v = 0; v < N; v++) {
                    if(boxCount[v] == 1) {
                        for(int i = 0; i < N; i++) {
                            int x = xBox + i%SIZE;
                            int y = yBox + i/SIZE;
                            if(isAllowed[x][y][v]) {
                                setAllowed(x, y, v+1);
                                isSolvedSection = true;
                            }
                        }
                    }
                }

            }
        }
        return isSolvedSection;
    }


    //implementation of the backtracking algorithm, with the recursion being managed
    //by calling the backtrackSolveOrder method, which in turns calls back backtrackSolve.
    //this method returns false if it was not able to place any values in the called grid location
    public boolean backtrackSolve(int x, int y) {

        for(int value = 1; value <= N; value++) {
            if(isValid(x, y, value)) {
                Grid[x][y] = value;

                boolean solved = backtrackSolveOrder();

                if(solved) {
                    return true;
                }
                else {
                    Grid[x][y] = 0;
                }
            }
        }

        return false;
    }


    //optimization of the backtrack algorithm, where values to recursively backtrack are called
    //based on the amount of possible values they can take (in increasing order).
    //this method returns true only if we've successfully filled every spot of the grid with valid values
    public boolean backtrackSolveOrder() {
        for (int x = 0; x < N; x ++) {
            for(int y = 0; y < N; y++) {
                validCount[x][y] = 0;
                if(isEmpty(Grid[x][y])) {
                    for(int v = 1; v <= N; v++) {
                        if(isValid(x, y, v)) {
                            validCount[x][y]++;
                        }
                    }
                }
            }
        }
        for (int count = 1; count <= N; count++) {
            for (int x = 0; x < N; x ++) {
                for(int y = 0; y < N; y++) {
                    if(isEmpty(Grid[x][y])) {
                        if(validCount[x][y] == count) {
                            return backtrackSolve(x, y);
                        }
                        if(validCount[x][y] == 0) return false;
                    }
                }
            }
        }
        return true;
    }


    //checks whether a grid has been fully solved, by looking through every spot and checking for empty (0) values
    public boolean isSolved() {
        for(int x = 0; x < N; x++) {
            for(int y = 0; y < N; y++) {
                if(isEmpty(Grid[x][y])) return false;
            }
        }
        return true;
    }

    //method to check whether a grid is empty
    public boolean isEmpty(int a) {
        return a == 0;
    }


    //method to check whether a value is a valid entry in the specified grid location
    public boolean isValid(int x, int y, int value) {
        return checkRowsAndCols(x, y, value) && checkBox(x, y, value);
    }

    //checks presence of a value in the row and column of the specified grid location
    public boolean checkRowsAndCols(int x, int y, int value) {
        for (int i = 0; i < N; i++) {
            if (value == Grid[x][i] || Grid[i][y] == value) {
                return false;
            }
        }
        return true;
    }

    //checks presence of a value in the box (subgrid) the specified grid location is
    public boolean checkBox(int x, int y, int value) {
        int xBox = (x / SIZE) * SIZE, yBox = (y / SIZE) * SIZE;
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (value == Grid[xBox + i][yBox + j]) return false;
            }
        }
        return true;
    }



    /*****************************************************************************/
    /* NOTE: YOU SHOULD NOT HAVE TO MODIFY ANY OF THE FUNCTIONS BELOW THIS LINE. */
    /*****************************************************************************/
 
    /* Default constructor.  This will initialize all positions to the default 0
     * value.  Use the read() function to load the Sudoku puzzle from a file or
     * the standard input. */
    public Sudoku( int size )
    {
        SIZE = size;
        N = size*size;

        Grid = new int[N][N];
        for( int i = 0; i < N; i++ )
            for( int j = 0; j < N; j++ )
                Grid[i][j] = 0;
    }


    /* readInteger is a helper function for the reading of the input file.  It reads
     * words until it finds one that represents an integer. For convenience, it will also
     * recognize the string "x" as equivalent to "0". */
    static int readInteger( InputStream in ) throws Exception
    {
        int result = 0;
        boolean success = false;

        while( !success ) {
            String word = readWord( in );

            try {
                result = Integer.parseInt( word );
                success = true;
            } catch( Exception e ) {
                // Convert 'x' words into 0's
                if( word.compareTo("x") == 0 ) {
                    result = 0;
                    success = true;
                }
                // Ignore all other words that are not integers
            }
        }

        return result;
    }


    /* readWord is a helper function that reads a word separated by white space. */
    static String readWord( InputStream in ) throws Exception
    {
        StringBuffer result = new StringBuffer();
        int currentChar = in.read();
        String whiteSpace = " \t\r\n";
        // Ignore any leading white space
        while( whiteSpace.indexOf(currentChar) > -1 ) {
            currentChar = in.read();
        }

        // Read all characters until you reach white space
        while( whiteSpace.indexOf(currentChar) == -1 ) {
            result.append( (char) currentChar );
            currentChar = in.read();
        }
        return result.toString();
    }


    /* This function reads a Sudoku puzzle from the input stream in.  The Sudoku
     * grid is filled in one row at at time, from left to right.  All non-valid
     * characters are ignored by this function and may be used in the Sudoku file
     * to increase its legibility. */
    public void read( InputStream in ) throws Exception
    {
        for( int i = 0; i < N; i++ ) {
            for( int j = 0; j < N; j++ ) {
                Grid[i][j] = readInteger( in );
            }
        }
    }


    /* Helper function for the printing of Sudoku puzzle.  This function will print
     * out text, preceded by enough ' ' characters to make sure that the printint out
     * takes at least width characters.  */
    void printFixedWidth( String text, int width )
    {
        for( int i = 0; i < width - text.length(); i++ )
            System.out.print( " " );
        System.out.print( text );
    }


    /* The print() function outputs the Sudoku grid to the standard output, using
     * a bit of extra formatting to make the result clearly readable. */
    public void print()
    {
        // Compute the number of digits necessary to print out each number in the Sudoku puzzle
        int digits = (int) Math.floor(Math.log(N) / Math.log(10)) + 1;

        // Create a dashed line to separate the boxes 
        int lineLength = (digits + 1) * N + 2 * SIZE - 3;
        StringBuffer line = new StringBuffer();
        for( int lineInit = 0; lineInit < lineLength; lineInit++ )
            line.append('-');

        // Go through the Grid, printing out its values separated by spaces
        for( int i = 0; i < N; i++ ) {
            for( int j = 0; j < N; j++ ) {
                printFixedWidth( String.valueOf( Grid[i][j] ), digits );
                // Print the vertical lines between boxes 
                if( (j < N-1) && ((j+1) % SIZE == 0) )
                    System.out.print( " |" );
                System.out.print( " " );
            }
            System.out.println();

            // Print the horizontal line between boxes
            if((i < N-1) && ((i+1) % SIZE == 0))
                System.out.println(line.toString());
        }
    }


    /* The main function reads in a Sudoku puzzle from the standard input, 
     * unless a file name is provided as a run-time argument, in which case the
     * Sudoku puzzle is loaded from that file.  It then solves the puzzle, and
     * outputs the completed puzzle to the standard output. */
    public static void main( String args[] ) throws Exception
    {
        InputStream in;
        if( args.length > 0 )
            in = new FileInputStream( args[0] );
        else
            in = System.in;

        // The first number in all Sudoku files must represent the size of the puzzle.  See
        // the example files for the file format.
        int puzzleSize = readInteger( in );
        if( puzzleSize > 100 || puzzleSize < 1 ) {
            System.out.println("Error: The Sudoku puzzle size must be between 1 and 100.");
            System.exit(-1);
        }

        Sudoku s = new Sudoku( puzzleSize );

        // read the rest of the Sudoku puzzle
        s.read( in );

        // Solve the puzzle.  We don't currently check to verify that the puzzle can be
        // successfully completed.  You may add that check if you want to, but it is not
        // necessary.

        s.solve();

        // Print out the (hopefully completed!) puzzle
        s.print();
    }
}
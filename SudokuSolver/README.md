#Sudoku Solver
This project was individually made as part of an algorithms class at McGill University. Its goal was to implement an algorithm in Java that would solve any Sudoku puzzle in the most efficient way possible.

This project was structured as an informal competition on who could design the fastest running algorithm, for which I got 2nd place.

###About the code
*note that this assignment didn't require that the complexity of our algorithms be calculated, therefore I'll be using execution time on my machine as a reference*

All we were taught in this class that was applicable to this project was the concept of recursive backtracking.

Implementing brute backtracking on a 25x25 sudoku usually resulted in around 45minutes of processing time. My final algorithm did this in a range of 1-15ms (for puzzles of type veryHard).

I achieved this performance by:
1. Implementing human strategies to solving the puzzle, before calling the backtracking algorithm once these get exhausted. 
2. Modifying the brute backtracking algorithm to use an ordering in its recursive calls, to reduce potential recursive calls.

Some of the code was provided by the course instructor (mostly relating to parsing the Sudoku from a text file). All code from lines 8 to 320 were written by me.

###Running the code
```bash
$ javac Sudoku.java
$ java Sudoku sudokuFileName.txt
```
Where `sudokuFileName.txt` can be any of the text files included in this repository for demo purposes. (or any other files of the same format)